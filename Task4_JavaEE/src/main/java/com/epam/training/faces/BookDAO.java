package com.epam.training.faces;

import com.epam.training.exceptions.DaoException;
import com.epam.training.model.beans.Book;
import com.epam.training.model.beans.User;
import java.util.List;

public interface BookDAO {
    /**
     * Request to demonstrate information about books (book name,
     * book author, pages number.
     *
     * @param user - user
     * @return - books list
     * @throws DaoException - DaoException
     */
    List<Book> getBooks(User user) throws DaoException;

    /**
     * Request to add new book (book name, book author, book author to the DB.
     *
     * @param user            - user
     * @param bookName        - book Name
     * @param bookAuthor      - book Author
     * @param bookDescription - book Description
     * @throws DaoException
     */
    void addBook(User user, String bookName, String bookAuthor, String bookDescription) throws DaoException;

    /**
     * Request to update book fields using book name like key.
     *
     * @param user            - user
     * @param bookName        - book Name
     * @param bookAuthor      - book Author
     * @param bookDescription - book Description
     * @throws DaoException
     */
    void updateBook(User user, String bookName, String bookAuthor, String bookDescription) throws DaoException;

    /**
     * Request to delete book(s) from DB by id.
     *
     * @param id - book id
     * @throws DaoException - DaoException
     */
    void deleteBookById(int id) throws DaoException;
}