package com.epam.training.faces;

import com.epam.training.exceptions.DaoException;
import com.epam.training.model.beans.User;

public interface UserDAO {
    /**
     * Request to return user (if exist) from DB according to parameters.
     * @param name - login
     * @param password - password
     * @return - user (if exist) from DB
     * @throws DaoException - DaoException
     */
    User getUser(String name, String password) throws DaoException;

    /**
     * Request to add user to DB.
     *
     * @param login- login
     * @param password - password
     * @return - user (if exist) from DB
     * @throws DaoException - DaoException
     */
    User addUser(String login, String password) throws DaoException;
}