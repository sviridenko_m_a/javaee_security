package com.epam.training.exceptions;

public class DaoException extends Exception {

    /**
     * Method returns exception description message
     * @param message - message
     */
    public DaoException(String message) {
        super(message);
    }

    /**
     * Method returns message with cause of exception
     * @param cause - cause
     */
    public DaoException(Throwable cause) {
        super(cause);
    }
}
