package com.epam.training.controllers;

import com.epam.training.constants.Constants;
import com.epam.training.constants.ConstantsJSP;
import com.epam.training.exceptions.DaoException;
import com.epam.training.faces.BookDAO;
import com.epam.training.model.beans.User;
import com.epam.training.model.impl.SQLBookImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UpdateController extends AbstractBaseController {

    private static final long serialVersionUID = 22L;
    private BookDAO bookDAO;

    /**
     * init DAO
     */
    @Override
    public void init() throws ServletException {
        super.init();
        bookDAO = new SQLBookImpl();
    }

    /**
     * Post method that updates (bookName, bookAuthor, bookDescription) or
     * add (bookAuthor, bookDescription) according username.
     *
     * @param request  - request
     * @param response - response
     * @throws ServletException - a servlet encounters difficulty
     * @throws IOException      - an input or output operation is failed or interpreted
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        User user = (User) request.getSession().getAttribute(ConstantsJSP.KEY_USER);
        String bookName = request.getParameter(ConstantsJSP.KEY_BOOK_NAME);
        String bookAuthor = request.getParameter(ConstantsJSP.KEY_BOOK_AUTHOR);
        String bookDescription = request.getParameter(ConstantsJSP.KEY_BOOK_DESCRIPTION);

        try {
            bookDAO.updateBook(user, bookName, bookAuthor, bookDescription);
            redirectPage(Constants.BOOK_CONTROLLER, response);
        } catch (DaoException e) {
            forwardError(Constants.SLASH + Constants.BOOK_PAGE, e.getMessage(), request, response);
        }
    }
}
