package com.epam.training.controllers.wrappers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class XSSRequestWrapper extends HttpServletRequestWrapper {

    public XSSRequestWrapper(HttpServletRequest servletRequest) {
        super(servletRequest);
    }

    /**
     * Method to cut string using stripXSS(String value) to prevent SQLInjections
     * @param parameter - injection string
     * @return - injection string without dangerous javaScript elements
     */
    @Override
    public String getParameter(String parameter) {
        String value = super.getParameter(parameter);
        return stripXSS(value);
    }

    /**
     * JavaScript elements cutter with javaScript elements patterns
     * @param value - string to cut
     * @return - string without javaScript patterns elements
     */
    private String stripXSS(String value) {
        if (value != null) {
            value = value.replaceAll(" ", "");
            value = value.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
            value = value.replaceAll("script", "");
        }
        return value;
    }
}
