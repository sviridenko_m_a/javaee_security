package com.epam.training.controllers;

import com.epam.training.constants.Constants;
import com.epam.training.constants.ConstantsJSP;
import com.epam.training.exceptions.DaoException;
import com.epam.training.faces.BookDAO;
import com.epam.training.model.beans.Book;
import com.epam.training.model.beans.User;
import com.epam.training.model.beans.enams.Role;
import com.epam.training.model.impl.SQLBookImpl;

import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@WebServlet(urlPatterns = {"/start"})
public class BookController extends AbstractBaseController {

    private static final long serialVersionUID = 3L;
    private AtomicBoolean flag = new AtomicBoolean(false);
    private BookDAO bookDAO;

    /**
     * init DAO
     */
    @Override
    public void init() throws ServletException {
        super.init();
        bookDAO = new SQLBookImpl();
    }

    /**
     * Get method that forwards to book.jsp
     *
     * @param request  - request
     * @param response - response
     * @throws ServletException - a servlet encounters difficulty
     * @throws IOException      - an input or output operation is failed or interpreted
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String stringFlag = getServletConfig().getInitParameter("flag");
        int sec = Integer.parseInt(getServletConfig().getInitParameter("seconds"));
        if (!flag.get() && stringFlag.equals("false")) {
            flag.set(true);
            throw new UnavailableException("Stopped for testing", sec);
        }

        User user = (User) request.getSession().getAttribute(ConstantsJSP.KEY_USER);
        try {
            List<Book> book = bookDAO.getBooks(user);
            request.setAttribute(ConstantsJSP.KEY_TASKS, book);

            if (user.getRole().equals(Role.USER_MANAGEMENT)) {
                forward(Constants.SLASH + Constants.BOOK_FOR_ADMIN_PAGE, request, response);
            } else if (user.getRole().equals(Role.USER)) {
                forward(Constants.SLASH + Constants.BOOK_PAGE, request, response);
            }

        } catch (DaoException e) {
            forwardError(Constants.SLASH + Constants.BOOK_PAGE, e.getMessage(), request, response);
        }
    }
}