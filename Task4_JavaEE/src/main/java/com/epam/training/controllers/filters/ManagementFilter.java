package com.epam.training.controllers.filters;

import com.epam.training.constants.Constants;
import com.epam.training.constants.ConstantsJSP;
import com.epam.training.model.beans.User;
import com.epam.training.model.beans.enams.Role;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ManagementFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession();
        User user = (User) session.getAttribute(ConstantsJSP.KEY_USER);
        if (Role.USER_MANAGEMENT != user.getRole()) {
            httpResponse.sendRedirect(Constants.SLASH + Constants.BOOK_PAGE);
            return;
        }
        filterChain.doFilter(request, response);
    }
}
