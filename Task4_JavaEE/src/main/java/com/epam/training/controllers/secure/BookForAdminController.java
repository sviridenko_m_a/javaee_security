package com.epam.training.controllers.secure;

import com.epam.training.constants.Constants;
import com.epam.training.constants.ConstantsJSP;
import com.epam.training.controllers.AbstractBaseController;
import com.epam.training.exceptions.DaoException;
import com.epam.training.faces.BookDAO;
import com.epam.training.model.beans.Book;
import com.epam.training.model.beans.User;
import com.epam.training.model.impl.SQLBookImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class BookForAdminController extends AbstractBaseController {
    private static final long serialVersionUID = 21L;
    private BookDAO bookDAO;

    /**
     * init DAO
     */
    @Override
    public void init() throws ServletException {
        super.init();
        bookDAO = new SQLBookImpl();
    }

    /**
     * Delete method that delete checked book & forwards to book.jsp
     *
     * @param request  - request
     * @param response - response
     * @throws ServletException - a servlet encounters difficulty
     * @throws IOException      - an input or output operation is failed or interpreted
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        User user = (User) request.getSession().getAttribute(ConstantsJSP.KEY_USER);

        if (request.getParameterValues("label") != null) {
            String[] checkboxes = request.getParameterValues("label");
            if (!request.getParameter("remove").isEmpty()) {
                try {
                    for (String checbox : checkboxes) {
                        bookDAO.deleteBookById(Integer.parseInt(checbox));
                    }
                    checkExistentBooks(user, request, response);
                } catch (DaoException e) {
                    forwardError(Constants.SLASH + Constants.BOOK_PAGE, e.getMessage(), request, response);
                }
            } else {
//      not necessary 'if/else' if you have only one button on the page.
                checkExistentBooks(user, request, response);
            }
        } else {
            checkExistentBooks(user, request, response);
        }
    }

    private void checkExistentBooks(User user, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Book> book = bookDAO.getBooks(user);
            request.setAttribute(ConstantsJSP.KEY_TASKS, book);
            forward(Constants.SLASH + Constants.BOOK_FOR_ADMIN_PAGE, request, response);
        } catch (DaoException | ServletException | IOException e) {
            forwardError(Constants.SLASH + Constants.BOOK_PAGE, e.getMessage(), request, response);
        }
    }
}