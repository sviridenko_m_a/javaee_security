package com.epam.training.controllers;

import com.epam.training.constants.Constants;
import com.epam.training.constants.ConstantsJSP;
import com.epam.training.controllers.util.CheckLoginPass;
import com.epam.training.exceptions.DaoException;
import com.epam.training.faces.UserDAO;
import com.epam.training.model.impl.SQLUserImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.ValidationException;
import java.io.IOException;

public class RegistrationController extends AbstractBaseController {

    private static final long serialVersionUID = 1L;
    private UserDAO userDAO;

    /**
     * init DAO
     */
    @Override
    public void init() throws ServletException {
        super.init();
        userDAO = new SQLUserImpl();
    }

    /**
     * Method to register person in DB and forward to login.jsp.
     * @param request - request
     * @param response - response
     * @throws ServletException - a servlet encounters difficulty
     * @throws IOException - an input or output operation is failed or interpreted
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String login = request.getParameter(ConstantsJSP.KEY_LOGIN);
        String password = request.getParameter(ConstantsJSP.KEY_PASSWORD);
        String password2 = request.getParameter(ConstantsJSP.KEY_PASSWORD_2);

        try {
            CheckLoginPass.passConfirm(password, password2);
            CheckLoginPass.CorrectLoginPass(login, password);
            userDAO.addUser(login, password);
            forward(Constants.SLASH + Constants.LOGIN_PAGE, request, response);
        } catch (DaoException | ValidationException e) {
            forwardError(Constants.SLASH + Constants.REGISTER_PAGE, e.getMessage(), request, response);
        }
    }
}