package com.epam.training.controllers.filters;

import com.epam.training.constants.ConstantsJSP;
import com.epam.training.model.beans.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession();
        User user = (User) session.getAttribute(ConstantsJSP.KEY_USER);
        if (user == null) {
            session.invalidate();
            httpResponse.sendRedirect(ConstantsJSP.JUMP_LOGIN);
            return;
        }
        filterChain.doFilter(request, response);
    }
}
