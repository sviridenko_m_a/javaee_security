package com.epam.training.controllers.util;

import com.epam.training.constants.ConstantsError;

import javax.xml.bind.ValidationException;

public class CheckLoginPass {

    /**
     * Method check login & password
     *
     * @param login    - login
     * @param password - password
     * @throws RuntimeException - shows message: - Login or password are empty.
     */

    public static boolean CorrectLoginPass(final String login, final String password) throws RuntimeException {
        if (login.equals("^[a-z][a-zA-Z0-9]*$")) {
            throw new RuntimeException(ConstantsError.ERROR_EMPTY);
        }
        if ("".equals(login.trim()) || "".equals(password.trim())) {
            throw new RuntimeException(ConstantsError.ERROR_EMPTY);
        }
        return true;
    }

    /**
     * Method confirm that two entered passwords are equals.
     * @param password - password
     * @param password2 - duplicate password
     * @throws ValidationException
     */
    public static void passConfirm(String password, String password2) throws ValidationException {
        if ((!password.equals(password2))) {
            throw new ValidationException(ConstantsError.ERROR_PASSWORD_MATCH);
        }
    }
}
