package com.epam.training.controllers;

import com.epam.training.constants.Constants;
import com.epam.training.constants.ConstantsJSP;
import com.epam.training.controllers.util.CheckLoginPass;
import com.epam.training.exceptions.DaoException;
import com.epam.training.faces.UserDAO;
import com.epam.training.model.beans.User;
import com.epam.training.model.impl.SQLUserImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginController extends AbstractBaseController {

    private static final long serialVersionUID = 5L;
    private UserDAO userDAO;

    /**
     * init DAO
     */
    @Override
    public void init() throws ServletException {
        super.init();
        userDAO = new SQLUserImpl();
    }

    /**
     * Post method that redirects page to com.epam.training.controllers/BookController.java
     * @param request - request
     * @param response - response
     * @throws ServletException - a servlet encounters difficulty
     * @throws IOException - an input or output operation is failed or interpreted
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String login = request.getParameter(ConstantsJSP.KEY_LOGIN);
        String password = request.getParameter(ConstantsJSP.KEY_PASSWORD);

        try {
            CheckLoginPass.CorrectLoginPass(login, password);
            User user = userDAO.getUser(login, password);
            request.getSession().setAttribute(ConstantsJSP.KEY_USER, user);
            redirectPage(Constants.BOOK_CONTROLLER, response);

        }catch (DaoException | RuntimeException e) {
            forwardError(Constants.SLASH + Constants.LOGIN_PAGE, e.getMessage(), request, response);
        }
    }
}
