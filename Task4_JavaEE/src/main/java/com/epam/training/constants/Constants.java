package com.epam.training.constants;

public final class Constants {

    public static final String SLASH = "/";
    public static final String BOOK_PAGE = "book.jsp";
    public static final String BOOK_FOR_ADMIN_PAGE = "secure/bookForAdmin.jsp";
    public static final String LOGIN_PAGE = "login.jsp";
    public static final String BOOK_CONTROLLER = "book";
    public static final String REGISTER_PAGE = "registration.jsp";
    public static final String KEY_ERROR_MESSAGE = "errorMessage";
}