package com.epam.training.constants;

public class ConstantsJSP {

    public static final String KEY_LOGIN = "login";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_PASSWORD_2 = "password2";
    public static final String KEY_USER = "user";
    public static final String KEY_TASKS = "book";
    public static final String JUMP_LOGIN = "/login.jsp";
    public static final String JUMP_REGISTER = "/registration.jsp";
    public static final String JUMP_START = "/start.jsp";
    public static final String JUMP_BOOK = "/book.jsp";
    public static final String JUMP_UPLOAD_BOOK_COVER = "/uploadBookCover.jsp";
    public static final String JUMP_ADD = "/addBook.jsp";
    public static final String JUMP_UPDATE = "/updateBook.jsp";
    public static final String JUMP_URL_LIST = "/urlList.jsp";
    public static final String KEY_BOOK_NAME = "bookName";
    public static final String KEY_BOOK_AUTHOR = "bookAuthor";
    public static final String KEY_BOOK_DESCRIPTION = "bookDescription";

}
