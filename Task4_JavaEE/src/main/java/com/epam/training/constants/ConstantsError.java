package com.epam.training.constants;

public class ConstantsError {

    public static final String ERROR_CONNECTION_DB = "Can't connect to database! Try later again.";
    public static final String ERROR_EMPTY = "Login or password are empty.";
    public static final String ERROR_LOGIN_NOT_EXIST = "Incorrect username!";
    public static final String ERROR_LOGIN_EXIST = "Login already exist!";
    public static final String ERROR_PASSWORD_MATCH = "Entered passwords do not match.";
    public static final String ERROR_WRONG_LOGIN_OR_PASSWORD = "Wrong login or password!";
    public static final String ERROR_EMPTY_BOOK_NAME = "Field bookName is empty.";

}
