package com.epam.training.model.beans;

import com.epam.training.model.beans.enams.Role;

import java.io.Serializable;

public class User implements Serializable {

    private static final long serialVersionUID = 92L;

    private String name;
    private String password;
    private Role role;

    public User() {
        super();
    }

    public User(String name, String password) {
        super();
        this.name = name;
        this.password = password;
    }

    public User(String name, String password, Role role) {
        super();
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        User user = (User) object;
        return Objects.equals(name, user.name) &&
                Objects.equals(password, user.password) &&
                role == user.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, password, role);
    }
}
