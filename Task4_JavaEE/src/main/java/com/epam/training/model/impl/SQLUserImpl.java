package com.epam.training.model.impl;

import com.epam.training.constants.ConstantsError;
import com.epam.training.exceptions.DaoException;
import com.epam.training.faces.UserDAO;
import com.epam.training.model.beans.User;
import com.epam.training.model.beans.enams.Role;
import com.epam.training.model.connections.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SQLUserImpl implements UserDAO {

    private static final Logger LOGGER = Logger.getLogger(SQLUserImpl.class.getName());

    private static final int LOGIN_FIELD_INDEX = 1;
    private static final int PSWD_FIELD_INDEX = 2;
    private static final int ROLE_FIELD_INDEX = 3;
    private static final String SELECT_LOGIN = "SELECT name, pswd, role FROM logins WHERE name = ?";
    private static final String INSERT_LOGIN = "INSERT INTO logins(name, pswd, role) VALUES (?, ?, ?)";
    private static final String ROLE_USER = "USER";

    public SQLUserImpl() {
    }

    /**
     * Method return user (if exist) from DB according to parameters
     *
     * @param login    - login
     * @param password - password
     * @return - user (if exist) from DB
     * @throws DaoException - DaoException
     */
    public User getUser(String login, String password) throws DaoException {

        try (Connection cn = ConnectionPool.getConnection();
             PreparedStatement ps = cn.prepareStatement(SELECT_LOGIN)) {
            ps.setString(LOGIN_FIELD_INDEX, login);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    String name = rs.getString(LOGIN_FIELD_INDEX);
                    String pswd = rs.getString(PSWD_FIELD_INDEX);
                    Role role = Role.valueOf(rs.getString(ROLE_FIELD_INDEX));

                    if (!pswd.equals(password)) {
                        throw new DaoException(ConstantsError.ERROR_WRONG_LOGIN_OR_PASSWORD);
                    }
                    return new User(name, password, role);
                } else {
                    throw new DaoException(ConstantsError.ERROR_LOGIN_NOT_EXIST);
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, ConstantsError.ERROR_CONNECTION_DB, e);
            throw new DaoException(e);
        }
    }

    /**
     * Method add user to DB.
     *
     * @param login- login
     * @param password - password
     * @return - login, password
     * @throws DaoException - DaoException
     */
    public User addUser(String login, String password) throws DaoException {

        try (Connection cn = ConnectionPool.getConnection();
             PreparedStatement psLoginSel = cn.prepareStatement(SELECT_LOGIN)) {
            psLoginSel.setString(LOGIN_FIELD_INDEX, login);

            try (ResultSet rs = psLoginSel.executeQuery();
                 PreparedStatement psLoginIns = cn.prepareStatement(INSERT_LOGIN)) {

                synchronized (SQLUserImpl.class) {
                    if (rs.next()) {
                        throw new DaoException(ConstantsError.ERROR_LOGIN_EXIST);
                    } else {
                        psLoginIns.setString(LOGIN_FIELD_INDEX, login);
                        psLoginIns.setString(PSWD_FIELD_INDEX, password);
                        psLoginIns.setString(ROLE_FIELD_INDEX, ROLE_USER);
                        psLoginIns.execute();
                    }
                }
                return new User(login, password);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, ConstantsError.ERROR_CONNECTION_DB, e);
            throw new DaoException(e);
        }
    }
}
