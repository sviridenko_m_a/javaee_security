package com.epam.training.model.helpers;

public class FibonacciNumberCalculate {

    /**
     * Method calculate Fibonacci number
     */
    public static long calculateFibonacci(long position) {
        if (position > 1) {
            return calculateFibonacci(position - 1) + calculateFibonacci(position - 2);
        }
        return position;
    }
}
