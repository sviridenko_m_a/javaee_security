package com.epam.training.model.impl;

import com.epam.training.exceptions.DaoException;
import com.epam.training.faces.BookDAO;
import com.epam.training.model.beans.Book;
import com.epam.training.model.beans.User;
import com.epam.training.model.connections.ConnectionPool;
import com.epam.training.model.helpers.BookFieldsDemonstrate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SQLBookImpl implements BookDAO {

    private final static String INSERT_BOOK = "INSERT INTO book(idLogin, name, author, bookDescription)" +
            " VALUES ((SELECT idLogin FROM logins WHERE name = ?), ?, ?, ?)";
    private final static String UPDATE_BOOK_FIELDS = "UPDATE book SET author = ? , bookDescription = ? WHERE (name = ?) AND "
            + "(idLogin = (SELECT idLogin FROM logins WHERE name = ?))";
    private final static String DELETE_BOOK = "DELETE FROM book WHERE (idBook = ?)";

    public SQLBookImpl() {
    }

    /**
     * Method demonstrate information about books (book name,
     * book author, pages number.
     *
     * @param user - user
     * @return = books list
     * @throws DaoException - DaoException
     */
    public List<Book> getBooks(User user) throws DaoException {
        BookFieldsDemonstrate bfd = new BookFieldsDemonstrate();
        List<Book> books = new ArrayList<>();

        try (
                Connection cn = ConnectionPool.getConnection();
                PreparedStatement ps = bfd.prepareStatement(cn, user.getName());
                ResultSet rs = ps.executeQuery()
        ) {

            while (rs.next()) {
                String name = rs.getString(1);
                String author = rs.getString(2);
                String bookDescription = rs.getString(3);
                String bookCoverName = rs.getString(4);
                int ID = rs.getInt(5);

                books.add(new Book(ID, name, author, bookDescription, bookCoverName));
            }
            return books;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Method add new books (book name, book author, book author to the DB.
     *
     * @param user            - user
     * @param bookName        - book Name
     * @param bookAuthor      - book Author
     * @param bookDescription - book Description
     * @throws DaoException
     */
    public void addBook(User user, String bookName, String bookAuthor, String bookDescription) throws DaoException {
        try (Connection cn = ConnectionPool.getConnection();
             PreparedStatement ps = cn.prepareStatement(INSERT_BOOK)) {

            ps.setString(1, user.getName());
            ps.setString(2, bookName);
            ps.setString(3, bookAuthor);
            ps.setString(4, bookDescription);
            ps.execute();

        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Method u update book fields using book name like key.
     *
     * @param user            - user
     * @param bookName        - book Name
     * @param bookAuthor      - book Author
     * @param bookDescription - book Description
     * @throws DaoException
     */
    public void updateBook(User user, String bookName, String bookAuthor, String bookDescription) throws DaoException {
        try (Connection cn = ConnectionPool.getConnection();
             PreparedStatement ps = cn.prepareStatement(UPDATE_BOOK_FIELDS)) {

            ps.setString(1, bookAuthor);
            ps.setString(2, bookDescription);
            ps.setString(3, bookName);
            ps.setString(4, user.getName());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Method delete book from DB by book id.
     * @param id - book id in DB
     * @throws DaoException - DaoException
     */
    public void deleteBookById(int id) throws DaoException {
        try (Connection cn = ConnectionPool.getConnection();
             PreparedStatement ps = cn.prepareStatement(DELETE_BOOK)) {
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

}
