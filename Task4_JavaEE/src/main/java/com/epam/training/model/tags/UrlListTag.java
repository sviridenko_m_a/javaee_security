package com.epam.training.model.tags;

import org.apache.log4j.Logger;

import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UrlListTag extends TagSupport {

    private static final Logger LOGGER = Logger.getLogger(UrlListTag.class.getName());
    private static final String URL = "url";
    private static final String RESOURCE = "resource";

    private Map<String, ? extends ServletRegistration> servletRegistrationsMap;
    private List<String> servletNamesList;
    private int i = 0;

    @Override
    public int doStartTag() throws JspException {
        servletRegistrationsMap = pageContext.getRequest().getServletContext().getServletRegistrations();
        servletNamesList = new ArrayList<>(servletRegistrationsMap.keySet());
        setAttributes();
        return EVAL_BODY_INCLUDE;
    }

    /**
     * Method output URL & RESOURCE to the urlList.jsp
     */
    @Override
    public int doAfterBody() throws JspException {
        if (setAttributes()) {
            try {
                pageContext.getOut().append("<br>");
            } catch (IOException e) {
                LOGGER.error("Output operation exception (lock UrlListTag/setAttributes())!", e);
            }
            return EVAL_BODY_AGAIN;
        }
        return SKIP_BODY;
    }

    /**
     * Method set attributes to the page context
     */
    private boolean setAttributes() {
        if (i < servletRegistrationsMap.size()) {
            String name = servletNamesList.get(i);
            pageContext.setAttribute(RESOURCE, name);
            pageContext.setAttribute(URL, servletRegistrationsMap.get(name).getMappings());
            i++;
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}