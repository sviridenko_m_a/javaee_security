package com.epam.training.model.beans;

import java.io.Serializable;

public class Book implements Serializable {
    private static final long serialVersionUID = 315L;

    private int bookID;
    private String bookName;
    private String bookAuthor;
    private String bookDescription;
    private String bookCoverName;

    public Book(int bookID, String bookName, String bookAuthor, String bookDescription) {
        this.bookID = bookID;
        this.bookName = bookName;
        this.bookAuthor = bookAuthor;
        this.bookDescription = bookDescription;
    }

    public Book(int bookID, String name, String author, String bookDescription, String bookCoverName) {
        this.bookID = bookID;
        this.bookName = name;
        this.bookAuthor = author;
        this.bookDescription = bookDescription;
        this.bookCoverName = bookCoverName;
    }

    public int getBookID() {
        return bookID;
    }

    public String getName() {
        return bookName;
    }

    public void setName(String name) {
        this.bookName = name;
    }

    public String getAuthor() {
        return bookAuthor;
    }

    public void setAuthor(String author) {
        this.bookAuthor = author;
    }

    public String getbookDescription() {
        return bookDescription;
    }

    public void setbookDescription(String pageNumber) {
        this.bookDescription = pageNumber;
    }

    public String getBookCoverName() {
        return bookCoverName;
    }

    public void setBookCoverName(String bookCoverName) {
        this.bookCoverName = bookCoverName;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Book book = (Book) object;
        return bookID == book.bookID &&
                Objects.equals(bookName, book.bookName) &&
                Objects.equals(bookAuthor, book.bookAuthor) &&
                Objects.equals(bookDescription, book.bookDescription) &&
                Objects.equals(bookCoverName, book.bookCoverName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookID, bookName, bookAuthor, bookDescription, bookCoverName);
    }

}
