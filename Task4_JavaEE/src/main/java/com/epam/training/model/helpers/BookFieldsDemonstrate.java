package com.epam.training.model.helpers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BookFieldsDemonstrate  {

    private static final String selectBooks = "SELECT  book.name, book.author, book.bookDescription, bookCoverName, book.idBook " +
            "FROM book INNER JOIN logins ON book.idLogin = logins.idLogin WHERE (logins.name = ?)";

    /**
     * PreparedStatement method to take information
     *  about books from DB
     * @param cn - connection
     * @param login - login
     * @return - prepareStatement
     * @throws SQLException - SQLException
     */
    public PreparedStatement prepareStatement(Connection cn, String login) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(selectBooks);
        setParameters(ps, login);
        return ps;
    }

    private void setParameters(PreparedStatement ps, Object... objects) throws SQLException {
        for (int i = 0; i < objects.length; i++) {
            ps.setObject(i + 1, objects[i]);
        }
    }
}