<%@ page import="com.epam.training.constants.ConstantsJSP" %>
<%@ taglib uri="/jstl/core" prefix="c"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF8">
    <title>Login</title>
</head>
<body>


    <% request.setAttribute("jump_start", ConstantsJSP.JUMP_START); %>
    <a href="<c:url value="${jump_start}"/>">Back to start page</a><br>
    <hr>

    <c:if test="${not empty errorMessage}">
        <c:out value="${errorMessage}"/><br><br>
    </c:if>

    <form name="loginForm" method="POST" action="<c:url value='/login'/>">
        Login:<br>
        <input type="text" name=<%= ConstantsJSP.KEY_LOGIN %> value="" autofocus title="Enter your Login!"><br>
        Password:<br>
        <input type="password" name=<%= ConstantsJSP.KEY_PASSWORD %> value="" title="Enter your Password!"><br><br>
        &nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="Login">
    </form>

</body>
</html>
