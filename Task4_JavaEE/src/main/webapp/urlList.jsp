<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="utils" uri="/WEB-INF/tld/exttags.tld" %>
<html>
<head>
    <title>UrlList</title>
</head>
<body>
<utils:listmapping>
    ${url} - ${resource}
</utils:listmapping><br>

Fibonacci Number: ${utils:calculateFibonacci(position)}
</body>
</html>
