<%@ page import="com.epam.training.constants.ConstantsJSP" %>
<%@ taglib uri="/jstl/core" prefix="c"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF8">
    <title>Main</title>
</head>
<body>

    <c:if test="${not empty errorMessage}">
        <c:out value="${errorMessage}"/><br>
    </c:if>

    <% request.setAttribute("jump_uploadBookCover", ConstantsJSP.JUMP_UPLOAD_BOOK_COVER); %>

<%--Text string.--%>
    <c:if test="${empty book}">
        <c:out value="There is no one Book."/><br>
    </c:if>

<%--Table.--%>
    <TABLE border="1">

<%--Table head.--%>
    <thead>
        <th>+</th>
        <th width="100">Book name</th>
        <th width="100">Book author</th>
        <th width="150">Book description</th>
        <th width="150">Book cover name</th>
    </thead>

    <tbody>
    <c:forEach var="book" items="${book}">
    <TR>
        <TD><input type="checkbox" name="label" value="${book.bookID}"></TD>
        <TD>${book.name}</TD>
        <TD>${book.author}</TD>
        <TD>${book.bookDescription}</TD>
        <TD>
        <a href="<c:url value="${jump_uploadBookCover}" />">Upload book cover</a>
        </TD>

    </TR>
    </c:forEach>
    </tbody>

    </TABLE><br><br>

    <% request.setAttribute("jump_add", ConstantsJSP.JUMP_ADD); %>
    <% request.setAttribute("jump_update", ConstantsJSP.JUMP_UPDATE); %>
    <% request.setAttribute("jump_urlList", ConstantsJSP.JUMP_URL_LIST); %>

    <a href="<c:url value="${jump_add}"/>">Add book</a>
    <a href="<c:url value="${jump_update}"/>">Update book</a>
    <a href="<c:url value="${jump_urlList}"/>">Url list</a>
</body>
</html>
