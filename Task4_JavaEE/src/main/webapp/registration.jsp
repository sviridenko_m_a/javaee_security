<%@ page import="com.epam.training.constants.ConstantsJSP" %>
<%@ taglib uri="/jstl/core" prefix="c"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF8">
    <title>Registration</title>
</head>
<body>

    <% request.setAttribute("jump_start", ConstantsJSP.JUMP_START); %>
    <a href="<c:url value="${jump_start}"/>">Back to start page</a><br>
    <hr>

    <c:if test="${not empty errorMessage}">
    <c:out value="${errorMessage}"/><p>
    </c:if>

    <form name="registerForm" method="POST" action="<c:url value='/registration'/>">
    Login:<br>
    <input type="text" name=<%= ConstantsJSP.KEY_LOGIN %> value="" autofocus><br><br>
    Password:<br>
    <input type="password" name=<%= ConstantsJSP.KEY_PASSWORD %> value=""><br><br>
    Repeat password:<br>
    <input type="password" name=<%= ConstantsJSP.KEY_PASSWORD_2 %> value=""><br><br>
&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="Register">
    </form>

</body>
</html>