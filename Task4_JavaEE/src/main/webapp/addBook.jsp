<%@ page import="com.epam.training.constants.ConstantsJSP" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>AbbBook</title>
</head>
<body>
<form name="addForm" action="add" method="post">
    book name: <input type="TEXT" name="<%= ConstantsJSP.KEY_BOOK_NAME %>"><br>
    book author: <input type="TEXT" name="<%= ConstantsJSP.KEY_BOOK_AUTHOR %>"><br>
    book description: <input type="TEXT" name="<%= ConstantsJSP.KEY_BOOK_DESCRIPTION %>"><br><br>
    <input type="submit" name="btn" value="Add book">
</form>

</body>
</html>
