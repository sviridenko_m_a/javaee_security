<%@ page import="com.epam.training.constants.ConstantsJSP" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>UpdateBook</title>
</head>
<body>

Use write book name to add or change book fields!
<br><br>

<form name="addForm" action="update" method="post">
    Update book fields:<br>
    book name: <input type="TEXT" name="<%= ConstantsJSP.KEY_BOOK_NAME %>"><br>
    book author: <input type="TEXT" name="<%= ConstantsJSP.KEY_BOOK_AUTHOR %>"><br>
    book description: <input type="TEXT" name="<%= ConstantsJSP.KEY_BOOK_DESCRIPTION %>"><br><br>
    <input type="submit" name="btn" value="Update book">
</form>

</body>
</html>
