package com.epam.training.controllers.wrappers;

import org.junit.BeforeClass;
import org.junit.Test;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class XSSRequestWrapperTest {

    private static HttpServletRequest servletRequest;
    private static String parameter;

    @BeforeClass
    public static void beforeClass() {
        servletRequest = mock(HttpServletRequest.class);
        parameter = "something";
    }

    @Test
    public void getParameterReturnNull() {
        when(servletRequest.getParameter(parameter)).thenReturn(null);
        String title = new XSSRequestWrapper(servletRequest).getParameter(parameter);
        assertNull(title);
    }

    @Test
    public void getParameter() {
        final Map<String, String> testData = new HashMap<>();
        testData.put("<script>", "&lt;&gt;");
        testData.put("< script>", "&lt;&gt;");
        testData.put("<script  >", "&lt;&gt;");

        testData.forEach((value, expectedValue) -> {
            when(servletRequest.getParameter(parameter)).thenReturn(value);

            String title = new XSSRequestWrapper(servletRequest).getParameter(parameter);
            assertEquals(expectedValue, title);
        });
    }
}