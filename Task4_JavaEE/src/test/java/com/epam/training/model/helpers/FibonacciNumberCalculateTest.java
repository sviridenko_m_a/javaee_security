package com.epam.training.model.helpers;

import org.junit.Test;

import static org.junit.Assert.*;

public class FibonacciNumberCalculateTest {

    @Test
    public void calculateFibonacciNotNull() {
        assertNotNull(FibonacciNumberCalculate.calculateFibonacci(3));
    }

    @Test
    public void calculateFibonacci() {
        long fibonacciNumber = FibonacciNumberCalculate.calculateFibonacci(3);
        assertEquals(2, fibonacciNumber);
    }

    @Test
    public void calculateFibonacciNull() {
        long fibonacciNumber = FibonacciNumberCalculate.calculateFibonacci(0);
        assertEquals(0, fibonacciNumber);
    }

    @Test
    public void calculateFibonacciOne() {
        long fibonacciNumber = FibonacciNumberCalculate.calculateFibonacci(1);
        assertEquals(1, fibonacciNumber);
    }
}